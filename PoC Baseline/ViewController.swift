//
//  ViewController.swift
//  PoC Baseline
//
//  Created by Lucas Eduardo do Prado on 08/06/18.
//  Copyright © 2018 Lucas Eduardo do Prado. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UICollectionViewDelegate {

    @IBOutlet weak var collectionView: UICollectionView!
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView.reloadData()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}


extension ViewController: UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    //2
    func collectionView(_ collectionView: UICollectionView,
                                 numberOfItemsInSection section: Int) -> Int {
        return 3
    }
    
    //3
    func collectionView(_ collectionView: UICollectionView,
                                 cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cv\(indexPath.row+1)", for: indexPath) as! CardCollectionViewCell
        print(String(describing: type(of: cell)))
        if indexPath.row > 0 {
            cell.infoLabek.attributedText = "o valor total da fatura está cadastrado em débito automático".asAttributed(withMinimumLineHeight: 20.0)
                // Configure the cell
        }
            return cell
        
    }
}
