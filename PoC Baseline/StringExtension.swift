//
//  StringExtension.swift
//  PoC Baseline
//
//  Created by Lucas Eduardo do Prado on 12/06/18.
//  Copyright © 2018 Lucas Eduardo do Prado. All rights reserved.
//

import Foundation
import UIKit


extension String {
    
    func asAttributed(withMinimumLineHeight lineHeight: CGFloat) -> NSAttributedString {
        let attrString = NSMutableAttributedString(string: self)
        let style = NSMutableParagraphStyle()
        style.minimumLineHeight = lineHeight
        
        attrString.addAttribute(.paragraphStyle,
                                value: style,
                                range: NSRange(location: 0, length: self.characters.count))
        return attrString
    }
}
